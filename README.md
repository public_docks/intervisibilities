![image alt ><](./icons/DOCKS_icon.png)

<h1 style="text-align: center;">DOCKS Intervisibility module

version 4.3.0</h1>

(readme last modification : *5-Jun-2024*)

License: GPL v2 (included)

Report any bugs to [DOCKS GitLab](https://gitlab.obspm.fr/public_docks/trajectories) at CENSUS, Paris Observatory-PSL.

Provide suggestions to [DOCKS mail](mailto:docks.contact@obspm.fr).

Try our remote service by sending the Subject: `[server]` to [DOCKS mail](mailto:docks.contact@obspm.fr), you will be replied with a path to your own folder on our server and README files on how to proceed.

Authors
=============================================

Development by Observatory of Paris - PSL Université Paris - 
Laboratory of Excellence ESEP, PSL Space Pole [CENSUS](https://census.psl.eu/?lang=en) 

Current developers: Guillaume BACHELIER, Boris SEGRET

Past contributors: Feliu LACREU (2023), Rashika JAIN (2022), T. DELRIEU (2020), Sebastien DURAND (2019), Janis DALBINS (2019)

&nbsp;

Features of the DOCKS Intervisibility module
=============================================

The DOCKS Intervisibility module computes the intervisibility of the satellite with the sun and ground stations. As a result, it provides compete information about the eclipses and ground station passes of a satellite for the complete mission duration. 

**Compatibility: **

* **This module has been successfully tested with Ubuntu 18 and Windows 11. If it does not work with other OS, the user can use remote service!**
* **This module has been successfully tested with Python 3.8, 3.9**

See the detailed documentation 2022-146-DOCKS-Intervisibility_V4.3 provided with the software.

&nbsp;

Installing DOCKS Intervisibility module
=============================================

**Before installing DOCKS Intervisibility, please install:** 

* Python >=3.8 and < 3.10

### Linux __and__ MS-Windows Users

1. Download the full module from the link provided in Gitlab > Repository > __Tags__ and decompress it. 

2. Open a windows prompt as administrator or linux terminal. Change the directory to `Intervisibility` and execute the following commands:

```
$ python3 -m pip install --upgrade pip setuptools wheel
$ python3 -m pip install --no-cache-dir -r requirements.txt
```

**Note**: If you want to use a specific version of python, then replace python3 by that version or path to python.

If your install goes smoothly then go to next section **Test the installation**. If there is an error then refer to the section **Common installation errors**

### Test the installation

One test case is provided in the folder. The inputs required for these tests are in the `input` folder. In order to test if the installation is successful, just run the test config file using the following command in a terminal/windows prompt open in the Intervisibility folder:
`$ python3 (path-to-intervisi/)main.py --config_file=(path-to-intervisi/)input/configuration_file.yaml`

`(path-to-intervisi/)` being the path to your DOCKS Intervisibility module folder if you opened the terminal/windows prompt outside of the Intervisibility directory. 

The program shall finish with "End of processing" and the output files should be saved in `output`.


### Common installation errors 

* **Linux  LLVM error**: If you have an LLVM error on linux, open a shell and write :
``` 
$ sudo apt install llvm-9
$ export LLVM_CONFIG=/usr/bin/llvm-config-9
```
* Sometimes, if after first installation the program does not run successfully, try to run it again.

* If some library is causing a problem, try to uninstall it and reinstall it using the commands:
```
$ python3 -m pip uninstall <library>
$ python3 -m pip install --no-cache-dir <library>
```

&nbsp;

Configuration File: .yaml
=============================================

From Intervisibility v2, the configuration must be given in a ".yaml" file. You can find an example of this file here: 
`(path-to-intervisi/)template.yaml`. The section of the lines after '#' are comments, not interpreted by the program. 

>==**WARNING** : Both absolute or relative paths can be used in the configuration file. If relative paths are used, they should be specified relative to the location of the configuration file.==

&nbsp;

Running DOCKS Intervisibility module
=============================================

In order to run the Intervisibility module you need to enter a command with the following pattern:
`$ python3 (path-to-intervisi/)main.py --config_file=(path-to-config/)CONFIG_FILE`

`(path-to-intervisi/)` : relative or absolute path to main.py (within DOCKS Intervisibility), no need to run python from any particular location. 

`(path-to-config/)` : relative or absolute path to your configuration file folder. 

For instance :

1. your current directory is : `/home/myfolder/IamHere/`
2. DOCKS Intervisibility is at : `/home/DOCKS/Intervisibility` 
3. the configuration file is at : `/home/config_folder/config_file.yaml` 

Then enter the commande line :
`$ python3 ../../DOCKS/Intervisibility/main.py --config_file=../../config_folder/config_file.yaml`

or you can also use the absolute path (with your OS' syntax, here in Linux): 
`$ python3 /home/DOCKS/Intervisibility/main.py --config_file=/home/config_folder/config_file.yaml`

&nbsp;

Inputs 
=============================================
In order to compute with DOCKS Intervisibility module you need to provide at least 5 inputs :

1. a configuration file (.yaml), see section above. 

2. a trajectory file (.txt), in the format given by the example provided in `(project/)DOCKS/Intervisibility/input/propagator/traj_test_file.txt`. The DOCKS Propagator module produces trajectories in this format. 

3. a ground stations catalog (.yaml).

4. a binary kernel (.spk), containing solar system bodies ephemeris, with the format de4XX.bsp. This file can be found in SPICE website of NAIF team.

5. a text kernel (.tls), containing leapseconds of previous years. This file can be found in SPICE website of NAIF team.

6. (optional) a star catalog (.txt), containing targeted stars with characteristics, in the format given by the example 'input/star_catalog.txt'
&nbsp;

Outputs 
=============================================

DOCKS Intervisibility module generates one output file:

1. A human readable **EVTF** file for the user that summarises the eclipses, occultations and ground station passes for the given mission trajectory.

2. A binary file made for the EPS module, with data about eclipses and sun ephemeris in the satellite sky.

&nbsp;

Thank you for your interest.

![image](icons/CENSUS_icon.png)
<h2 style="text-align: center;">DOCKS by CENSUS</h2>
<img src="./icons/Observatoire_de_Paris-CoMarquage-RGB_Ultra_violet.png" alt="drawing" width="400" style="float: right;"/>


# from requirements import requirements
try:
    from core.launching_IV import launching_intervisibility
    import click
except ImportError:
    # requirements()
    import click
    from core.launching_IV import launching_intervisibility

@click.command()
@click.option(
    "--config_file",
    help="config file of Intervisibility module relative to exe--> yaml extension",
)
@click.option(
    "--disable",
    is_flag=True,
    default=False,
    help="Disable flag. Default is False, use --disable to set to True."
)
# if python script.py --config_file=config.yaml --disable, disable = True
# if python script.py --config_file=config.yaml, disable = False

def intervisibility_cmd(config_file, disable):
    try:
        launching_intervisibility(config_file, disable)
    except ImportError:
        # requirements()
        launching_intervisibility(config_file, disable)

intervisibility_cmd()


#if __name__ == "__main__":
#    intervisibility_cmd()


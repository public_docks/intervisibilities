import numpy as np
import spiceypy as spice
from math import cos, sin, sqrt
from numpy.linalg import norm
from core.bodies_utils import IV_process
from core.defined_bodies import name

def vector_GS_Earth(GS_components, time_array):
    """
    This function gives the vector between the Ground Station and the Earth Center (in J2000 ref)
    :param GS_components: (list) Parameters of the ground station from the ground_stations.yaml file
    :param time_array: (Astropy time, size: N) Vector of time
    :return: earth_GS_vector: (list) Ephem of GS from earth in J2000 ref
    """
    Latitude = GS_components[1]
    Longitude = GS_components[2]
    Height = GS_components[3]
    r = earth_radius(Latitude) + Height*10**(-3)

    # convert GS parameters into cartesian system
    x = r * np.cos(np.deg2rad(Latitude)) * np.cos((np.deg2rad(Longitude)))
    y = r * np.cos(np.deg2rad(Latitude)) * np.sin((np.deg2rad(Longitude)))
    z = r * np.sin(np.deg2rad(Latitude))
    
    # convert ITRF93 (Earth Fixed reference frame) into ICRS/J2000
    x1 = []
    y1 = []
    z1 = []
    for i in range(len(time_array)):
        trans_matrix = spice.pxform("ITRF93", "J2000", time_array[i])
        M = np.dot(trans_matrix, [x, y, z])
        x1.append(M[0])
        y1.append(M[1])
        z1.append(M[2])
    
    earth_GS_vector = np.array((x1, y1, z1)).T
    
    return earth_GS_vector


def earth_radius(latitude):
    """
    This function give the radius of the Earth depending on the latitude
    :param latitude: (float) Considered latitude
    :return: radius: (float) Radius of the Earth at the selected latitude
    """
    latitude = np.deg2rad(latitude)
    equatorial_radius = 6378.1370
    polar_radius = 6356.7523

    a = ((equatorial_radius ** 2) * cos(latitude)) ** 2 + ((polar_radius ** 2) * sin(latitude)) ** 2
    b = (equatorial_radius * cos(latitude)) ** 2 + (polar_radius * sin(latitude)) ** 2
    rad = sqrt(a / b)
    return rad


def angle_sat_zenithGS(unit_earth_GS_vector, GS_sat_vector):
    # compute angle between the direction of the satellite from the GS and the zenith of the GS
    gamma_angle = np.zeros((len(unit_earth_GS_vector)))
    for i in range(len(unit_earth_GS_vector)):
        a = np.dot(unit_earth_GS_vector[i], GS_sat_vector[i])
        b = norm(GS_sat_vector[i])
        gamma_angle[i] = np.arccos(a / b)

    return gamma_angle


def earth_occultation_status(
        hiding_bodies,
        center,
        time_vector,
        sat_trajectory,
        custom_bodies_list
):
    """
    This function gives the Intervisibility criterion with given hiding bodies and specifically the Earth as target
    :param hiding_bodies: (list) Considered hiding bodies
    :param center: (string) Name of the center of the reference frame of the satellite
    :param time_vector: (Astropy time, size: N) Vector of time
    :param sat_trajectory: (array, size: N*5) Coordinates of the satellite trajectory
    :param custom_bodies_list: (list[[str,int,float]], size: N*3 with N number of custom_bodies implemented) contains for each custom_body its name, naif_id and radius
    :return: final_earth_occultation_condition: (array, size: N) Intervisibility criterion
    """
    target = 'earth'
    list_earth_occ_condition = np.expand_dims(time_vector, axis=1)

    for hiding_body in hiding_bodies:
        hiding_body = name(hiding_body, custom_bodies_list)
        if target != hiding_body:
            earth_occulation_condition, sat_target_ephem = IV_process(target, hiding_body, center, time_vector, sat_trajectory, custom_bodies_list)
            earth_occulation_condition = np.expand_dims(earth_occulation_condition, axis=1)
            list_earth_occ_condition = np.append(list_earth_occ_condition, earth_occulation_condition, axis=1)

    list_earth_occ_condition = np.delete(list_earth_occ_condition, 0, axis=1)

    final_earth_occultation_condition = np.ones((len(list_earth_occ_condition)))
    occultation_status = 0
    for i in range(len(list_earth_occ_condition)):
        if occultation_status in list_earth_occ_condition[i]:
            final_earth_occultation_condition[i] = 0

    return final_earth_occultation_condition

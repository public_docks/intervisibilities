import spiceypy as spice
import numpy as np
from core.Intervisibility_utils_function import spice_spkezr_wrapper, light_time_delay
from core.defined_bodies import radius
from numpy.linalg import norm

def hiding_body_angle(
    hiding_body,
    center,
    time_array,
    sat_coord,
    custom_bodies_list
    ):

    """
    This function uses the trajectory of the satellite and the ephemerides of the three bodies
    to compute the position of the target and the hiding body relative to the observer
    :param target: (string) Name of the selected target, no case sensitivity
    :param hiding_body: (string) Name of the selected hiding body, no case sensitivity
    :param time_array: (Astropy time, size: N) Vector of time
    :param sat_coord: (array, size: N*5) Data trajectory of the satellite,
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :param center: (string) Name of the center of the reference frame of the satellite, no case sensitivity
    :param custom_bodies_list: (list[[str,int,float]], size: N*3 with N number of custom_bodies implemented) contains for each custom_body its name, naif_id and radius
    :return: sat_target_ephem: (array, size: N*3) Cartesian coordinates of the target in satellite reference frame
             sat_hiding_body_ephem: (array, size: N*3) Cartesian coordinates of the hiding body in satellite reference frame
    """

    # convert the time vector into SPICE time (TDB seconds past the J2000 epoch)
    time_ephem = spice.str2et(time_array.isot)
    wrapper_time_vector = tuple(time_ephem)
    
    # convert body names into naif ID
    naif_id_body_center = spice.bods2c(str(center))
    ephem_body_center = spice_spkezr_wrapper(naif_id_body_center, wrapper_time_vector)
    
    
    naif_id_hiding_body = spice.bods2c(str(hiding_body))
    ephem_hiding_body = spice_spkezr_wrapper(naif_id_hiding_body, wrapper_time_vector)

    # transform coordinates in SSB reference frame to satellite reference frame
    sat_hiding_body_ephem = np.zeros((len(sat_coord), 3))

    for i in range(len(sat_coord)):
        sat_hiding_body_ephem[i] = - sat_coord[i, 2:5] - ephem_body_center[i][0:3] + ephem_hiding_body[i][0:3]
        
    time_delta_hiding_body = light_time_delay(sat_hiding_body_ephem)
    time_shifted_hiding_body = time_ephem - time_delta_hiding_body
    
    wrapper_time_vector_shifted = tuple(time_shifted_hiding_body)    
    ephem_hiding_body = spice_spkezr_wrapper(naif_id_hiding_body, wrapper_time_vector_shifted)

    sat_hiding_body_ephem_shifted = np.zeros((len(sat_coord), 3))
    
    beta_angle = np.zeros((len(sat_coord)))
    for i in range(len(sat_coord)):
        sat_hiding_body_ephem_shifted[i] = - sat_coord[i, 2:5] - ephem_body_center[i][0:3] + ephem_hiding_body[i][0:3]
        beta_angle[i] = np.arcsin(radius(hiding_body, custom_bodies_list)/(norm(sat_hiding_body_ephem_shifted[i])))

    return beta_angle, sat_hiding_body_ephem_shifted


def star_ICRS_direction(ra, dec):
    """
    This functions transforms the spherical coordinates of a star expressed in right ascension and declination
    into a unit vector containing rectangular cartesian coordinates
    :param ra: (float) Right ascension of the star
    :param dec: (float) Declination of the star
    :return: star_vector_direction: (array, size: 3) Direction of the star in cartesian frame
    """

    ra = np.deg2rad(ra)
    dec = np.deg2rad(dec)
    r = 1
    x = r * np.cos(ra) * np.cos(dec)
    y = r * np.sin(ra) * np.cos(dec)
    z = r * np.sin(dec)

    star_vector_direction = np.array((x, y, z))

    return star_vector_direction



def separation_angle(star_vector_direction, sat_hiding_body_ephem):
    """
    This function compute the separation angle between a star and a hiding body
    :param star_vector_direction: (array, size: 3) Direction of the star in cartesian frame
    :param sat_hiding_body_ephem: (array, size: N*5) Cartesian coordinates of the hiding body in satellite reference frame
    :return: separation_angle: (array, size: N) Angle between the star and the hiding body
    """
    separation_angle = np.zeros(len(sat_hiding_body_ephem))
    
    for i in range(len(sat_hiding_body_ephem)):
        a = np.dot(star_vector_direction, sat_hiding_body_ephem[i])
        b = norm(star_vector_direction) * norm(sat_hiding_body_ephem[i])
        separation_angle[i] = np.arccos(a/b)
    
    return separation_angle

import datetime
import os
import sys
from typing import List

import numpy
import numpy as np
import inspect


# def variable_name_decorator(func):
#     def wrapper(*args, **kwargs):
#         # Get the name of the first argument passed to the function
#         frame = inspect.currentframe().f_back
#         variable_name = frame.f_locals[inspect.getargvalues(frame)[0][0]]
#
#         # Call the original function with the variable name as an additional argument
#         return func(*args, variable_name=variable_name, **kwargs)
#     return wrapper


def custom_file_detection(file, flag):
    if file is None:
        raise FileNotFoundError(f"No {flag} file were given")
    try:
        f = open(file)
    except FileNotFoundError:
        raise FileNotFoundError(f"'{file}' for {flag} does not exist")
    else:
        pass

# @variable_name_decorator
def custom_value_detection(variable, flag):
    """
    Check if a variable is defined and has a truthy value.

    :param variable: Any variable to be checked.
    :param variable_name: The name of the variable for better error reporting.
    :raises ValueError: If the variable is not defined or has a falsy value.
    """
    if variable is None or not variable:
        raise ValueError(f"'{flag}' should be defined and have a truthy value.")



def convert_string_to_naifid(body_name):
    """
    This function convert body name into NAIF ID
    :param body_name: (string) Body name
    :return: (int) NAIF ID
    """
    body_name = body_name.lower()
    naif_ids = {'sun': 10, 'earth': 399, 'moon': 301, 'mercury': 199, 'venus': 299,
                'mars': 4, 'jupiter': 5, 'saturn': 6, 'uranus': 7, 'neptune': 8, 'pluto': 9}

    if body_name not in naif_ids:
        raise ValueError('this name is not supported')
    return naif_ids[body_name]


def cic_intervisibility(
    time_event,
    event_type,
    path_EVTF: str,
    trajectory_file,
    configuration_file
) -> None:
    """
    This fonction writes the events in file in CIC format

    :param time_event: (float vector, size: K) mjd date of event
    :param event_type: (string vector, size K) COMINxxxxx and COMEGxxxxx (xxxxx = ground station number)
    :param orbit_sat_file: (string) Name and path of Satellite orbit files
    :param path_inter: (string) Name and path for intervisibility CIC file

    :return: No return
    """


    cic = "CIC_MEM_VERS = 1.0"
    origin = "ORIGINATOR = DOCKS / CENSUS / LabEx ESEP - Paris Observatory - PSL University Paris"

    comment = [f"COMMENT = Intervisibility for config : {configuration_file}",
               f"COMMENT = Intervisibility for trajectory : {trajectory_file}",
               "COMMENT = Columns : DATE MJDday + MJDseconds + EVENT + EventIndex"]
    header = [
        "USER_DEFINED_PROTOCOL = NONE",
        "USER_DEFINED_CONTENT = OEF",
        "USER_DEFINED_SIZE = 2",
        "USER_DEFINED_TYPE = STRING",
        "USER_DEFINED_UNIT = [n/a]",
        "TIME_SYSTEM = UTC",
    ]
    sequence = ["%s", "%s", "%s"]
    data_time_temp = np.array(
        [np.int32(time_event), np.float32((time_event % 1) * 60 * 60 * 24)]
    )

    data_time = [[*map("{:.0f}".format, data_time_temp[0,:])], [*map("{:15.6f}".format, data_time_temp[1,:])]]
    data_time = np.array(data_time).T

    data_science = []
    for i in range(len(event_type)):
        data_science.append([f'{event_type[i]: >15}'])
    write_ccsds(
        path_EVTF, cic, origin, comment, header, sequence, data_time, data_science
    )


def write_ccsds(sat_quat_path_out, cic, origin, comment, header, sequence, data_time, data_science,
                separator='\t', mode="normal"):
    """
    write cic file

    :param sat_quat_path_out:(string) : Output path for new CIC file
    :param cic:(string) : CIC string Line
    :param origin:(string) : String origin Name Line
    :param comment:(string vector) : String List of Comment, line by line
    :param header:(string vector) : String List of other line
    :param sequence:(string vector) : String List for defined writing formats
                    (ex: for time + 3 data: ["%d","%.6f","%.8f","%.8f","%.8f"])
                    (ex: for time + 1 string data: ["%s","%s","%s"])
    :param data_time:(Nx2, float) : All Time, julian day and second
    :param data_science:(NxM, float) : All Data, M x data
    :param mode:(string) : "normal" write full file, "stream" write new data line in file

    :return: No have return
    """


    if mode == "normal":
        if len(data_time) == len(data_science):
            data = []
            for i in range(len(data_time)):
                data.append(np.concatenate((data_time[i], data_science[i])))

            #if no event is detected the program breaks so we check if events exist before continuing
            if data:
                if len(data[0]) == len(sequence):

                    f0 = open(sat_quat_path_out, 'w')

                    # create first header :
                    f0.write(cic + "\n")
                    f0.write(datetime.datetime.now().strftime("CREATION_DATE = %Y-%m-%dT%H:%M:%S.%f")[:-3] + "\n")
                    f0.write(origin + "\n")

                    # create principal header
                    f0.write("\n" + "META_START" + "\n" + "\n")
                    for i in range(len(comment)):
                        f0.write(comment[i] + "\n")
                    f0.write("\n")
                    for i in range(len(header)):
                        f0.write(header[i] + "\n")
                    f0.write("\n" + "META_STOP" + "\n" + "\n")

                    # write data
                    for i in range(len(data_science)):
                        np.savetxt(f0, [data[i]], delimiter=separator, fmt=sequence)

                    f0.close()
                else:
                    print("Data_time + data_science size :", (len(data_time[0]) + len(data_science[0])))
                    print("sequence size :", len(sequence))
                    raise IndexError("Data_time + data_science size is not the same size as sequence")
            else:
                print("\nNo events were found")
        else:
            print("Data_time size :", len(data_time))
            print("Data Science size :", len(data_science))
            raise IndexError("Data_time and data_science have not the same size")
    elif mode == "stream":
        f0 = open(sat_quat_path_out, 'a')
        data = np.append(np.concatenate((data_time, data_science)))
        np.savetxt(f0, [data], delimiter=separator, fmt=sequence)
        f0.close()


def read_cic(
    fname: str, ind: slice = slice(None, None), meta: bool = True
) :
    """
    Open trajectory file with cic format to import the state vector as
    ndarray. It extracts state vector columns withing the given range (default
    is the whole state vector)
    :param fname: (string) Name of the file
    :param ind:
    :param meta:
    :return: data: (array, size: N*M) Data contained in the CIC file
    """

    T = []
    if not os.path.isfile(fname):
        fname_save = fname
        fname = f"rsc/traj/{fname.split('/')[-1]}"
        if not os.path.isfile(fname):
            fname = fname_save
    with open(fname) as F:
        for L in F:
            L = L.strip()
            if L == "META_STOP":
                meta = False
                continue
            if meta or not L:
                continue
            T.append([float(e) for e in L.split()[ind]])
    data = numpy.asarray(T)
    return data





def write_cic(fname: str, data, header: str):
    """Write trajectory to cic file."""
    if data.shape[1] == 8:
        fmt = "%d\t%d\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f"
    else:
        sys.exit(
            "Error: expected trajectory shape (N, 8) but %s is given" % (data.shape,)
        )
    numpy.savetxt(fname, data, fmt=fmt, header=header, comments="")


def create_header(
    object_name: str = "",
    center_name: str = "",
    ref_frame: str = "",
    time_system: str = "",
) -> str:
    """Create header for cic file."""
    date = datetime.datetime.now().isoformat()
    return f"""CIC_OEM_VERS = 2.0
CREATION_DATE = {date}
ORIGINATOR = docks_helper.py

META_START

COMMENT         DOCKS Propagator version 4.0.0
COMMENT         Propagator method : rk4_module
COMMENT         Colomn 1:2 Unit : Time (MJD)
COMMENT         Colomn 3:5 Unit : Position (KM)
COMMENT         Colomn 6:8 Unit : Velocity (KM/S)
COMMENT         Colomn 9:11 Unit : Acceleration (KM/S^2)

OBJECT_NAME     = {object_name}
OBJECT_ID       = CORPS

CENTER_NAME     = {center_name}
REF_FRAME       = {ref_frame}
TIME_SYSTEM     = {time_system}

META_STOP
"""


def create_config(
    fname: str,
    name: str,
    gravitational_perturbations: List,
    new_perturbations: List,
    duration: float,
    time_step: float,
):
    """Create config file for DOCKS propagator."""
    config = f"""**Propagator_START**
Initial_conditions_file=initial-condition-files/init-{name}.txt
Output_file_name={name}.txt
Output_directory=output-trajectories
Init_input=sun;ICRF;MJD;KM;KM/S
Gravitational_perturbations={gravitational_perturbations}
Ephem_type=jpl
New_Perturbations={new_perturbations}
Complex_grav_model_activated=False
Complex_grav_model_bodies=[]
Non_gravitational_perturbations=[]
Number_of_bodies={len(gravitational_perturbations)}
Number_of_new_bodies={len(new_perturbations)}
Number_of_cgm_bodies=0
Number_of_non_gravitational_perturbations=0
Propagation_param=rk4_module;0;0
Time=Duration;{duration};0:0:0.0
Time_step=Hours;{time_step};0;0
Number_of_steps={duration * 24 / time_step}
output_format=sun;ICRF;MJD;KM;KM/S;KM/S^2;1
**Propagator_STOP**"""
    with open(fname, "w") as F:
        F.write(config)







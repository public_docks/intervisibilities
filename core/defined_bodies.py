bodies_data = {
    10: {"Name": "sun", "Radius": 696342.0},
    199: {"Name": "mercury", "Radius": 2440.0},
    299: {"Name": "venus", "Radius": 6051.84},
    399: {"Name": "earth", "Radius": 6371.01},
    499: {"Name": "mars", "Radius": 3389.92},
    599: {"Name": "jupiter", "Radius": 69911.0},
    699: {"Name": "saturn", "Radius": 58232.0},
    799: {"Name": "uranus", "Radius": 25362.0},
    899: {"Name": "neptune", "Radius": 24624.0},
    999: {"Name": "pluto", "Radius": 1188.3},
    301: {"Name": "moon", "Radius": 1737.53},
    401: {"Name": "phobos", "Radius": 11.16},
    402: {"Name": "deimos", "Radius": 6.3},
    501: {"Name": "io", "Radius": 1821.5},
    502: {"Name": "europa", "Radius": 1560.8},
    503: {"Name": "ganymede", "Radius": 2631.2},
    504: {"Name": "callisto", "Radius": 2410.},
    603: {"Name": "tethys", "Radius": 531.0},
    604: {"Name": "dione", "Radius": 561.4},
    605: {"Name": "rhea", "Radius": 763.5},
    606: {"Name": "titan", "Radius": 2574.8},
    608: {"Name": "iapetus", "Radius": 734.5},
    701: {"Name": "ariel", "Radius": 578.9},
    702: {"Name": "umbriel", "Radius": 584.7},
    703: {"Name": "titania", "Radius": 788.9},
    704: {"Name": "oberon", "Radius": 761.4},
    801: {"Name": "triton", "Radius": 1352.6},
    901: {"Name": "charon", "Radius": 606.0},
}

global custom_bodies_list
    
def get_body_radius(input_value):
    input_value = str(input_value)
    if input_value.isdigit():
        planet_number = int(input_value)
        if planet_number in bodies_data:
            return bodies_data[planet_number].get("Radius")
    elif input_value.lower() in [data["Name"] for data in bodies_data.values()]:
        for naif_id, data in bodies_data.items():
            if data["Name"] == input_value.lower():
                return data.get("Radius")
    return None

def get_body_name(input_value):
    if isinstance(input_value, int):
        for naif_id, data in bodies_data.items():
            if naif_id == input_value and "Name" in data:
                return data["Name"]
    elif isinstance(input_value, str):
        for naif_id, data in bodies_data.items():
            if "Name" in data and data["Name"].lower() == input_value.lower():
                return data["Name"]
    return None

def radius(body, custom_bodies_list):
    radius = get_body_radius(body)
    if radius == None:  #if we don't find it, we search in the custom_bodies_list
        for i in range(len(custom_bodies_list)):
            if str(body)==str(custom_bodies_list[i][0]) or str(body)==str(custom_bodies_list[i][1]):
                return float(custom_bodies_list[i][2])
    return radius

def name(body, custom_bodies_list):
    name = get_body_name(body)
    if name == None:  #if we don't find it, we search in the custom_bodies_list
        for i in range(len(custom_bodies_list)):
            if str(body)==str(custom_bodies_list[i][0]) or str(body)==str(custom_bodies_list[i][1]):
                return custom_bodies_list[i][0]
    return name

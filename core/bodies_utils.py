import numpy as np
from numpy.linalg import norm
import spiceypy as spice
from core.Intervisibility_utils_function import spice_spkezr_wrapper
from core.defined_bodies import radius
from core.Intervisibility_utils_function import light_time_delay, light_time_delay_HB, light_time_delay_target
def ICRS_coord(
    target,
    hiding_body,
    center,
    time_array,
    sat_coord
    ):

    """
    This function uses the trajectory of the satellite and the ephemerides of the three bodies
    to compute the position of the target and the hiding body relative to the observer

    :param target: (string) Name of the selected target, no case sensitivity
    :param hiding_body: (string) Name of the selected hiding body, no case sensitivity
    :param time_array: (Astropy time, size: N) Vector of time
    :param sat_coord: (array, size: N*5) Data trajectory of the satellite,
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :param center: (string) Name of the center of the reference frame of the satellite, no case sensitivity
    :return: sat_target_ephem: (array, size: N*3) Cartesian coordinates of the target in satellite reference frame
             sat_hiding_body_ephem: (array, size: N*3) Cartesian coordinates of the hiding body in satellite reference frame
    """

    # convert the time vector into SPICE time (TDB seconds past the J2000 epoch)
    time_ephem = spice.str2et(time_array.isot)

    # convert body names into naif ID
    naif_id_body_center = spice.bods2c(str(center))
    naif_id_target = spice.bods2c(str(target))
    naif_id_hiding_body = spice.bods2c(str(hiding_body))

    wrapper_time_vector = tuple(time_ephem)
    # recover ephemerides of each considered body from kernels
    ephem_body_center = spice_spkezr_wrapper(naif_id_body_center, wrapper_time_vector)
    ephem_target = spice_spkezr_wrapper(naif_id_target, wrapper_time_vector)
    ephem_hiding_body = spice_spkezr_wrapper(naif_id_hiding_body, wrapper_time_vector)


    # transform coordinates in SSB reference frame to satellite reference frame
    sat_target_ephem = np.zeros((len(sat_coord), 3))
    sat_hiding_body_ephem = np.zeros((len(sat_coord), 3))

    for i in range(len(sat_coord)):
        sat_target_ephem[i] = - sat_coord[i, 2:5] - ephem_body_center[i][0:3] + ephem_target[i][0:3]
        sat_hiding_body_ephem[i] = - sat_coord[i, 2:5] - ephem_body_center[i][0:3] + ephem_hiding_body[i][0:3]

    return sat_target_ephem, sat_hiding_body_ephem


def light_time_correction(
    sat_target_ephem,
    sat_hiding_body_ephem,
    target,
    hiding_body,
    time_array,
    sat_coord,
    center
    ):

    """
    This function compute the distance between the considered bodies (target and hiding body)
    and the observer. Then compute the delay of the light travel from the bodies to the observer and
    recompute the position of the two bodies into observer reference frame with the delay added.

    :param sat_target_ephem: (array, size: N*3) Cartesian coordinates of the target in satellite reference frame
    :param sat_hiding_body_ephem: (array, size: N*3) Cartesian coordinates of the hiding body in satellite reference frame
    :param target: (string) Name of the selected target, no case sensitivity
    :param hiding_body: (string) Name of the selected hiding body, no case sensitivity
    :param time_array: (Astropy time, size: N) Vector of time
    :param sat_coord: (array, size: N*5) Data trajectory of the satellite,
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :param center: (string) Name of the center of the reference frame of the satellite, no case sensitivity
    :return: sat_target_ephem: (array, size: N*3) Cartesian coordinates of the target in satellite reference frame with delay
             sat_hiding_body_ephem: (array, size: N*3) Cartesian coordinates of the hiding body in satellite reference frame with delay

    """ 
    time_delta_hiding_body = light_time_delay_HB(sat_hiding_body_ephem)
    time_ephem = spice.str2et(time_array.isot)

    # convert name string into naif ID
    naif_id_body_center = spice.bods2c(str(center))
    naif_id_target = spice.bods2c(str(target))
    naif_id_hiding_body = spice.bods2c(str(hiding_body))

    # computation of ephemeris of the center body
    ephem_body_center = spice_spkezr_wrapper(naif_id_body_center, tuple(time_ephem))

    # creation of the new time vector with the delay for each body
    time_shifted_hiding_body = time_ephem - time_delta_hiding_body

    # computation of ephemerides of target and hiding body with delay in SSB frame
    ephem_hiding_body = spice_spkezr_wrapper(naif_id_hiding_body, tuple(time_shifted_hiding_body))
    ephem_target_t = spice_spkezr_wrapper(naif_id_target, tuple(time_shifted_hiding_body))
    
    sat_target_ephem_2 = np.zeros((len(sat_coord), 3))
    sat_hiding_body_ephem_2 = np.zeros((len(sat_coord), 3))
    for i in range(len(sat_coord)):
        sat_target_ephem_2[i] = ephem_target_t[i][0:3]
        sat_hiding_body_ephem_2[i] = ephem_hiding_body[i][0:3]

    time_delta_target = light_time_delay_target(sat_hiding_body_ephem_2,sat_target_ephem_2)
    time_shifted_target = time_shifted_hiding_body - time_delta_target
    
    ephem_target = spice_spkezr_wrapper(naif_id_target, tuple(time_shifted_target))

    sat_target_ephem = np.zeros((len(sat_coord), 3))
    sat_hiding_body_ephem = np.zeros((len(sat_coord), 3))
    # transformation from SSB frame into satellite reference frame
    for i in range(len(sat_coord)):
        sat_target_ephem[i] = - sat_coord[i, 2:5] - ephem_body_center[i][0:3] + ephem_target[i][0:3]
        sat_hiding_body_ephem[i] = - sat_coord[i, 2:5] - ephem_body_center[i][0:3] + ephem_hiding_body[i][0:3]

    return sat_target_ephem, sat_hiding_body_ephem


def occultation_condition(
    target,
    hiding_body,
    sat_target_ephem,
    sat_hiding_body_ephem,
    custom_bodies_list,
    ):
    """
    This function computes the Intervisibility criterion using the coordinates of the target
    and the hiding body in the satellite reference frame

    :param target: (string) Name of the selected target, no case sensitivity
    :param hiding_body: (string) Name of the selected hiding body, no case sensitivity
    :param sat_target_ephem: (array, size: N*3) Cartesian coordinates of the target in satellite reference frame with delay
    :param sat_hiding_body_ephem: (array, size: N*3) Cartesian coordinates of the hiding body in satellite reference frame with delay
    :param custom_bodies_list: (list[[str,int,float]], size: N*3 with N number of custom_bodies implemented) contains for each custom_body its name, naif_id and radius
    :return: final_condition: (list[float [0, 1]], size: N) Intervisibility criterion (0 -> Occultation, 1 -> Intervisibility)
    TODO: Optimize the last for-loop
    """
    alpha_angle = np.zeros(len(sat_target_ephem))
    beta_angle = np.zeros(len(sat_target_ephem))
    gamma_angle = np.zeros(len(sat_target_ephem))

    # creation of the angles
    # alpha & beta -> apparent radius of target and hiding body
    # gamma -> angle between the unit vector of target and hiding body


    for i in range(len(sat_target_ephem)):
        alpha_angle[i] = np.arcsin(radius(target, custom_bodies_list) / norm(sat_target_ephem[i]))
        beta_angle[i] = np.arcsin(radius(hiding_body, custom_bodies_list) / norm(sat_hiding_body_ephem[i]))

        a = np.dot(sat_target_ephem[i], sat_hiding_body_ephem[i])
        b = norm(sat_target_ephem[i])* norm(sat_hiding_body_ephem[i])
        gamma_angle[i] = np.arccos(a/b)

    # intervisibility criterion
    condition1 = gamma_angle > - alpha_angle + beta_angle

    # condition1 = False = 0 -> occultation
    # condition1 = True = 1 -> visibility

    distance_sat_hiding_body = np.linalg.norm(sat_hiding_body_ephem, axis = 1)
    distance_sat_target = np.linalg.norm(sat_target_ephem, axis = 1)

    condition2 = distance_sat_hiding_body > distance_sat_target

    # condition2 == False == 0 -> target behind the HB -> occultation
    # condition2 == True == 1 -> target in front of the HB -> visibility

    final_condition = np.zeros((len(condition1)))

    for i in range(len(condition1)):
        if condition2[i]:
            final_condition[i] = 1
        else:
            final_condition[i] = condition1[i]

    return final_condition


def IV_process(target, hiding_body, center, time_vector, sat_trajectory, custom_bodies_list):

    """
    This fynction do the intervisibility process by executing the three following functions sequentially:
    ICRS_coord() -> stellar_aberration_correction() -> occultation_condition()
    :param target: (string) Name of the selected target, no case sensitivity
    :param hiding_body: (string) Name of the selected hiding body, no case sensitivity
    :param center: (string) Name of the center of the reference frame of the satellite, no case sensitivity
    :param time_vector: (Astropy time, size: N) Vector of time
    :param sat_trajectory: (array, size: N*5) Data trajectory of the satellite,
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :param custom_bodies_list: (list[[str,int,float]], size: N*3 with N number of custom_bodies implemented) contains for each custom_body its name, naif_id and radius
    :return: angle_condition: (list[float [0, 1]], size: N) Intervisibility criterion (0 -> Occultation, 1 -> Intervisibility)
             sat_target_ephem: (array, size: N*3) Cartesian coordinates of the target in satellite reference frame with delay
    """

    sat_target_ephem, sat_hiding_body_ephem = ICRS_coord(
        target, hiding_body,
        center,
        time_vector,
        sat_trajectory)

    sat_target_ephem, sat_hiding_body_ephem = light_time_correction(
        sat_target_ephem,
        sat_hiding_body_ephem,
        target, hiding_body,
        time_vector,
        sat_trajectory,
        center)

    angle_condition = occultation_condition(
        target, hiding_body,
        sat_target_ephem,
        sat_hiding_body_ephem,
        custom_bodies_list)

    return angle_condition, sat_target_ephem

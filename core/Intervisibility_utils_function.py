import numpy as np
from numpy.linalg import norm
import spiceypy as spice
from functools import lru_cache
from math import ceil
from scipy import interpolate
import os
from pathlib import Path
import pandas as pd
from astropy.time import Time
import random

def generate_unique_naif_id(): #generate a random and non used naif_id
    while True:
        naif_id = random.randint(-1000000, -1) 
        try:
            body_name = spice.bodc2n(naif_id)
        except spice.stypes.SpiceyError:
            break
        else:
            continue
    return naif_id

def create_spkfile(kernels_path, body_name, ephem_file): 
    """
    This function create a spk file .bsp for a custom_body and delete the existing one
    :param kernels_path: (str) Path to the kernels repository
    :param body_name: (str) Name of our new custom_body
    :param ephem_file: (str) Path to the trajectory file of this custom_body
    :return: body_naif_id: (int) Naif_id generated for the custom_body and associated to the ephem data in the new spk file
    """
    
    bsp_filename = body_name + '.bsp'
    
    # we delete the old spk file for our custom_body if it already existed
    if Path(kernels_path + '/' + bsp_filename).is_file():
        spice.kclear()
        os.remove(kernels_path + '/' + bsp_filename)
        print(f"The existing spk file '{bsp_filename}' has been deleted")
    try:
        handle = spice.spkopn(kernels_path + '/' + bsp_filename, 'Type 13 BSP file', 0) #create the new spk file
        print(f"The spk file '{bsp_filename}' has been created created")
    except Exception as e:
        print(f"Error on the creation of the spk file '{bsp_filename}': {e}")
    
    # we open all the spk files
    for path, subdirs, files in os.walk(kernels_path):
        for name in files:
            file_path = os.path.join(path, name)
            if name.endswith((".bsp", ".tls", ".BSP", ".tm", ".TM")) and not(name.endswith((bsp_filename))):
                spice.furnsh(str(file_path))

    #we take some data from the ephem_file : center_name and ref_frame, for the custom_body trajectory  
    with open(ephem_file, 'r') as file:
        for i in range(18): #on regarde que les premières lignes
            line = next(file).strip() 
            if line.startswith("CENTER_NAME"):
                center = str(line.split("= ")[1].strip()) 
            elif line.startswith("REF_FRAME"):
                ref_frame = str(line.split("= ")[1].strip())
 
    body_naif_id = generate_unique_naif_id()
    spice.boddef(body_name, body_naif_id)
    center_id = spice.bodn2c(center)
    
    #we read now the ephem data
    data = pd.read_csv(ephem_file, sep='\s+', skiprows=19, header=None)
    data_array = np.array(data)
    
    mjd_day = data_array[:, 0]
    mjd_seconds = data_array[:, 1]
    mjd_array = mjd_day + mjd_seconds / 86400
    time_array = [Time(mjd, format='mjd').isot for mjd in mjd_array]
    time_spice = spice.str2et(time_array)
    time_liste = time_spice.tolist()
        
    ephem = data_array[:, 2:]
    ephem_liste = ephem.tolist()

    spice.spkw13(handle, body_naif_id, center_id, 'J2000', time_liste[0], time_liste[-1], 'NONE', 3, len(time_liste), ephem_liste, time_liste)  #we put ref_frame = J2000 as ICRF is not supported

    spice.spkcls(handle) #we close it
    
    return body_naif_id

@lru_cache
def spice_spkezr_wrapper(naif_id, wrapper_time_vector):
    time_vector = np.array(wrapper_time_vector)

    body_ephemeris, _ = spice.spkezr(str(naif_id), time_vector, 'J2000', 'NONE', '0')

    return body_ephemeris

def arange(start: float, end: float, step: float):
    """
    Implementation of numpy.arange to include end
    """
    return np.linspace(start, end, ceil((end - start) / step) + 1)


def interpolate_trajectory(
    time,
    target,
    trajectory_flag
):
    """
    This function interpolate cartesian trajectory according to time
    :param time: (array, size: N) New time vector used for interpolation
    :param target: (array, size: N*5) Data trajectory of the satellite
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :return: target_new: (array, size: N*5) Interpolated data trajectory of the satellite
    """

    time = np.squeeze(time)
    
    if len(time[0].shape) == 0:
        time = mjd1_to_mjd2(time)

    time_old = target[:, :2]
    time_old = np.squeeze(time_old)
    time1d_old = mjd2_to_mjd1(time_old)
    time1d = mjd2_to_mjd1(time)
    target_new = np.zeros((time.shape[0], target.shape[1]))
    target_new[:, :2] = time

    if trajectory_flag == False:
        for i in range(2, 5):
            interp = interpolate.interp1d(time1d_old, target[:, i], kind="cubic")
            target_new[:, i] = interp(time1d)

    else:
        component = ["x vector", "y vector", "z vector"]
        for i, comp in zip(range(2, 5), component):
            interp = interpolate.interp1d(time1d_old, target[:, i], kind="cubic")
            target_new[:, i] = interp(time1d)
        
        print(f"\n")
        
    return target_new


def mjd2_to_mjd1(time):
    """Convert time format from MJD two columns to MJD one column"""
    return time[:, 0] + time[:, 1] / 86400


def mjd1_to_mjd2(time):
    """Convert time format from MJD one column to MJD two columns"""
    col0 = time // 1
    col1 = (time - time // 1) * 86400
    col0 = col0.reshape((col0.size, 1))
    col1 = col1.reshape((col1.size, 1))
    return np.concatenate((col0, col1), axis=1)



def light_time_delay(sat_body_ephemeris):
    c = 299792.458
    time_delta = np.zeros(len(sat_body_ephemeris))
    for i in range(len(time_delta)):
        time_delta[i] = norm(sat_body_ephemeris[i, :])/c
    return time_delta

def light_time_delay_target(sat_body_ephemeris, sat_target_ephemeris):
    c = 299792.458
    time_delta = np.zeros(len(sat_body_ephemeris))
    for i in range(len(time_delta)):
        time_delta[i] = norm(sat_target_ephemeris[i, :]-sat_body_ephemeris[i, :])/c
    return time_delta

def light_time_delay_HB(sat_body_ephemeris):
    c = 299792.458
    time_delta = np.zeros(len(sat_body_ephemeris))
    for i in range(len(time_delta)):
        time_delta[i] = norm(sat_body_ephemeris[i, :])/c
    return time_delta


def epochs_to_neutralize(light_time_delay_target, light_time_delay_HB, time_vector):
    highest_delay = light_time_delay_target if light_time_delay_target > light_time_delay_HB else light_time_delay_HB

    starting_time = time_vector[0]
    nb_epochs = 0
    k = 0
    while (time_vector[k] - starting_time) < highest_delay:
        nb_epochs += 1
        k += 1

    return nb_epochs


def event_creation(event_type_list, types, target = '', index = ''):
    """
    This functions creates the events depending on the target selected and its type
    :param event_type_list: (array, size: N) Type of event detected (1 --> egress, -1 --> ingress)
    :param types: (string) Type of the current target: body, ground station or star
    :param target: (string) Name of the target if any
    :param index: (string) Index of the target if any
    :return: event_type: (list[string]) Formatted list of encountered events with types, status, target (or index) and number of the event
    TODO: change double if statements by two functions
            1. event number (ECL, OCC)
            2. object index (COM, STAR)
    """

    if target.lower() == "sun":
        scheme_in = "ECL/IN"
        scheme_eg = "ECL/EG"
    elif types == "body":
        scheme_in = f"OCC/IN/{target.upper()}"
        scheme_eg = f"OCC/EG/{target.upper()}"
    elif types == "GS":
        scheme_in = f"COM/IN/{index}"
        scheme_eg = f"COM/EG/{index}"
    elif types == "star":
        scheme_in = f"OCC/IN/STAR/{index}"
        scheme_eg = f"OCC/EG/STAR/{index}"
    else:
        print("types not detected")

    event_type = []

    iterator_occin = 1
    iterator_occeg = 1


    #To avoid swapped IN/EG for communications we need to use the opposite
    if types == 'GS':
        for i in range(len(event_type_list)):
            event_type_list[i] *= -1

    for i in range(len(event_type_list)):
        if event_type_list[i] == -1:
            event_type.append(f"{scheme_in:25}\t" + f"{iterator_occin:05d}")
            iterator_occin += 1

        elif event_type_list[i] == 1:

            if iterator_occin == 1:
                iterator_occeg = 0

            event_type.append(f"{scheme_eg:25}\t" + f"{iterator_occeg:05d}")
            iterator_occeg += 1

    return event_type

def event_check(condition, time_vector):
    """
    This function detect events by checking a change of value in the Intervisibility criterion
    :param condition: (array, size: N) Intervisibility criterion
    :param time_vector: (Astropy time, size: N) Vector of time
    :return: event_time: (list[float]) Time of events
             event_type: (list[float]) Type of events
    """
    event_time = []
    event_type = []

    for i in range(len(condition)-1):
        if condition[i+1] - condition[i] != 0:
            event_time.append(time_vector[i].mjd)
            event_type.append(condition[i+1] - condition[i])
    return event_time, event_type


def event_junction(event_time, event_type):
    """
    This function keeps the highest priority events if series of same multiple events are detected
    priority -> multiple ingress: first ingress kept
                multiple egress: last egress kept

    :param event_time: (array, size: N) Time of events
    :param event_type: (array, size: N) Type of events (1 --> egress, -1 --> ingress)
    :return: event_time: (array, size: N) Time of events
             event_type: (array, size: N) Type of events
    """
    """
    Test case:
    event_time = [*range(0, 28)]
    event_type = [-1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1, 1, -1, -1, -1, 1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, 1, 1]
    a, b = event_junction(event_time, event_type)
    """

    to_delete = []
    for i in range(len(event_type) - 1):
        status = event_type[i + 1] - event_type[i]
        if status == 0:
            if event_type[i] == -1:
                to_delete.append(i + 1)
            elif event_type[i] == 1:
                to_delete.append(i)

    event_type = np.delete(event_type, to_delete)
    event_time = np.delete(event_time, to_delete)

    # to_delete.reverse()
    #
    # for i in to_delete:
    #     event_type.pop(i)
    #     event_time.pop(i)
    return event_time, event_type



def event_sort(time_event, event_type):
    """
    This function sort events, times and types, chronologically
    :param time_event: (array, size: N) Time of events
    :param event_type: (array, size: N) Type of events (1 --> egress, -1 --> ingress)
    :return: time_event: (array, size: N) Time of events
             event_type: (array, size: N) Type of events
    """
    time_event = np.array(time_event)
    event_type = np.array(event_type)
    new_order = np.argsort(time_event)
    event_type = event_type[new_order]
    time_event = time_event[new_order]
    return time_event, event_type


def check_event_precision(
    time_event,
    interpolation_pos_input,
    minimum_status_duration,
    event_time_accuracy
    ):
    """
    This function interpolate the trajectory and the time vector with the proper time accuracy
    :param time_event: (list[float]) Time of events
    :param interpolation_pos_input: (array, size: N*5) Coordinates of the satellite trajectory
    :param minimum_status_duration: (float) Minimum time duration for the start and end of an event
    :param event_time_accuracy: (float) Time accuracy for detection of an event
    :return: interpolation_trajectory: (array, size: N*5) Interpolated data trajectory of the satellite
             new_time: (array, size: N) New time vector
    """
    time_event = np.squeeze(time_event)

    for counter, value in enumerate(time_event):
        if counter == 0:
            new_time = arange(
                float(str(value)),
                float(str(value + (minimum_status_duration-1) / 86400)),
                event_time_accuracy / 86400,
                )
        else:
            new_time = np.append(
                new_time,
                arange(
                    float(str(value)),
                    float(str(value + (minimum_status_duration-1) / 86400)),
                    event_time_accuracy / 86400,
                    ),
            )

    last_time = interpolation_pos_input[-1, 0] + (interpolation_pos_input[-1, 1] / 86400)
    if new_time[-1] > last_time:
        to_delete = []
        for index, time_epoch in enumerate(new_time):
            if time_epoch > last_time:
                to_delete.append(index)

        new_time = np.delete(new_time, to_delete, axis=0)

    new_time = np.reshape(new_time, (-1, 1))
    interpolation_trajectory = interpolate_trajectory(new_time, interpolation_pos_input, trajectory_flag=False)

    new_time = mjd1_to_mjd2(new_time)
    new_time = np.round(new_time, 0)

    new_time[:, 1] = np.around(new_time[:, 1], 0)

    new_time = mjd2_to_mjd1(new_time)

    return interpolation_trajectory, new_time

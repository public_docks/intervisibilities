import time
import pickle as pck
import spiceypy as spice

from core.docks_helper import (
    read_cic,
    cic_intervisibility,
    custom_file_detection,
    custom_value_detection
    )
from astropy.time import Time
from core.Intervisibility_utils_function import (
    event_sort,
    interpolate_trajectory,
    arange,
    create_spkfile,
    )
from core.Intervisibility import body_intervisibility, ground_stations_intervisibility, stars_intervisibility
from math import isclose
import yaml
import os

def launching_intervisibility(config_file, disable):
    """
    This function unpack the data coming from the configuration file then executes the three Intervisibility process,
    and finally create the event file
    :param config_file: (string) File containing information and conditions of the script
    :param disable: (Boolean) Disable flag for tqdm progress bars. Default is False, use --disable to set to True
    """
    #--------------------initialisation of files and variables--------------------
    
    start_time = time.time()
    original_path = os.getcwd()
    
    launching_path = original_path
    config_path = str(launching_path) + '/' + config_file

    with open(config_path) as config_yaml:
        info_input = yaml.safe_load(config_yaml)
    
    #custome_body part
    custom_bodies = info_input['custom_bodies']
    kernels_path = info_input['kernels_path']
    
    custom_bodies_list = [] 
    if custom_bodies != None:
        for custom_body in custom_bodies:
            body_name = str(custom_body[0])
            ephem_file = str(custom_body[1])
            body_radius = float(custom_body[2])
        
            body_naif_id = create_spkfile(kernels_path, body_name, ephem_file) #create a new spk file and return the naif_id generated for the body
            custom_bodies_list.append([body_name, body_naif_id, body_radius]) #we save the data of the custom_bodies to use later their radius
        
    spice.kclear() #unload every kernel (so we don't read spk already opened in write access)
    if custom_bodies != None:
        for couple in custom_bodies_list:
            spice.boddef(couple[0], couple[1]) #we associate custom_bodies name and naif_id
    
    GS_file = info_input['ground_stations_catalog']
    with open(GS_file) as config_yaml:
        GS_input = yaml.safe_load(config_yaml)


    GS_number = info_input['ground_stations']
    star_catalog = info_input["star_catalog"]


    root = info_input['kernels_path']
    for path, subdirs, files in os.walk(root):
        for name in files:
            file_path = os.path.join(path, name)
            if name.endswith((".bsp", ".tls", ".BSP", ".tm", ".TM", ".bpc")):
                spice.furnsh(str(file_path))

    num_kernels = spice.ktotal('all')
    print(f"Loaded Kernels : ")
    # Loop through the loaded kernels and print their names
    for i in range(num_kernels):
        kernel_info = spice.kdata(i, 'ALL')
        kernel_name = kernel_info[0]
        print('\t-',kernel_name)

    os.chdir(original_path)
    print()

    # input and output files, paths and names
    trajectory_file = info_input["trajectory_file"]
    output_file_body = info_input["Event_file"]
    output_file_EPS = info_input["EPS_output_file"]
    custom_file_detection(trajectory_file, flag = "trajectory")
        
    # time properties
    minimum_status_duration = info_input["minimum_status_duration"]
    custom_value_detection(minimum_status_duration, flag = "minimum_status_duration")
    event_time_accuracy = info_input["event_time_accuracy"]
    custom_value_detection(event_time_accuracy, flag = "event_time_accuracy")

    # --------------------parameters of bodies--------------------
    center = info_input["center"]
    custom_value_detection(center, flag="center")

    hiding_bodies = info_input["hiding_bodies"]
    custom_value_detection(hiding_bodies, flag="hiding_bodies")

    """"""
    # --------------------defined bodies--------------------
    # definition of target body, hiding body and the center (center of reference frame of the trajectory)
    targets = info_input["solar_system_bodies"]


    print(f"Hiding bodies : {hiding_bodies}")
    print(f"Center of reference frame : {center}")
    print(f"Targets : {targets}")
    print(f"Ground stations index : {GS_number}")
    print(f"Star_catalog : {star_catalog}")

    #--------------------transformation of satellite trajectory--------------------

    # extraction of the trajectory data
    trajectory_data = read_cic(trajectory_file)
    
    # split data into time vector and trajectory vector (position and velocity)
    time_vector = trajectory_data[:, 0] + trajectory_data[:, 1]/86400
    traj_vector = trajectory_data[:, :5]
    
    # if the timestep of the trajectory is close to the event duration
    # -> we interpolate the trajectory data with the minimum status duration as timestep
    if not isclose(
            int(float(str((time_vector[1] - time_vector[0]) * 86400))),
            minimum_status_duration,
            abs_tol=1,
    ):
        new_time = arange(
            float(str(time_vector[0])),
            float(str(time_vector[-1])),
            minimum_status_duration / 86400,
        )
    
        interpolation_trajectory = interpolate_trajectory(new_time, traj_vector, trajectory_flag = True)
        new_time = Time(new_time, format='mjd')

    else:
        interpolation_trajectory = traj_vector
        new_time = Time(time_vector, format='mjd')

    # --------------------global variables event set--------------------
    global_event_time = []
    global_event_type = []


    #--------------------intervisibility for bodies--------------------

    if targets:
        time_event, event_type, sun_eph_sat_sky, sun_inter_sat, time_vector = body_intervisibility(targets, hiding_bodies, new_time, interpolation_trajectory, center, minimum_status_duration, event_time_accuracy, custom_bodies_list)

        time_event, event_type = event_sort(time_event, event_type)

        global_event_time.extend(time_event)
        global_event_type.extend(event_type)
    
    #EPS output file
    if ('sun' in targets or 'Sun' in targets or 'SUN' in targets or 10 in targets or '10' in targets) and output_file_EPS!=None and output_file_EPS!='': #sun in targets
        with open(output_file_EPS, "wb") as file:
            pck.dump(sun_eph_sat_sky, file)
            pck.dump(sun_inter_sat, file)
            pck.dump(time_vector, file)
        

    #--------------------intervisibility for ground stations--------------------

    if GS_number:
        time_event, event_type = ground_stations_intervisibility(
                GS_number,
                GS_input,
                new_time,
                interpolation_trajectory,
                hiding_bodies,
                center,
                minimum_status_duration,
                event_time_accuracy,
                custom_bodies_list,
                disable
                )
        global_event_time.extend(time_event)
        global_event_type.extend(event_type)

    #--------------------intervisibility for star catalog--------------------

    if star_catalog:
        time_event, event_type = stars_intervisibility(
                star_catalog,
                new_time,
                interpolation_trajectory,
                hiding_bodies,
                center,
                minimum_status_duration,
                event_time_accuracy,
                custom_bodies_list)

        time_event, event_type = event_sort(time_event, event_type)

        global_event_time.extend(time_event)
        global_event_type.extend(event_type)


    global_event_time, global_event_type = event_sort(global_event_time, global_event_type)
    cic_intervisibility(global_event_time, global_event_type, output_file_body, trajectory_file, config_file)

    end_time = time.time()

    print(f"\nEnd of processing")
    print(f"Elapsed time = {end_time - start_time:.2f}s")
    # print(spice_spkezr_wrapper.cache_info())

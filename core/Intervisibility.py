import numpy as np
from core.Intervisibility_utils_function import (
check_event_precision,
event_creation,
event_sort,
event_check,
event_junction,
)

from core.ground_stations_utils import vector_GS_Earth, angle_sat_zenithGS, earth_occultation_status
from core.docks_helper import read_cic
from astropy.time import Time
import spiceypy as spice
from core.stars_utils import star_ICRS_direction, hiding_body_angle, separation_angle
from core.bodies_utils import IV_process
from tqdm import tqdm
from numpy.linalg import norm
from core.defined_bodies import name
from math import cos, sin, sqrt
from numpy.linalg import norm
import spiceypy as spice
from core.Intervisibility_utils_function import spice_spkezr_wrapper, interpolate_trajectory
from astropy.coordinates import CartesianRepresentation,SkyCoord
import sys
from io import StringIO

import warnings
warnings.filterwarnings("ignore")


def body_intervisibility(
    targets,
    hiding_bodies,
    time_vector,
    sat_trajectory,
    center,
    minimum_status_duration,
    event_time_accuracy,
    custom_bodies_list
    ):

    """
    This function compute intervisibility between selected body and satellite with
    regard to the possible occultations by a hiding object

    :param target: (string) Name of the selected target, no case sensitivity
    :param hiding_body: (string) Name of the selected hiding body, no case sensitivity
    :param new_time: (Astropy time, size: N) Vector of time
    :param sat_trajectory: (array, size: N*5) Data trajectory of the satellite,
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :param center: (string) Name of the center of the reference frame of the satellite, no case sensitivity
    :param minimum_status_duration: (integer) Minimum duration needed to consider an event
    :param event_time_accuracy: (integer) Precision of the dating of events
    :param custom_bodies_list: (list[[str,int,float]], size: N*3 with N number of custom_bodies implemented) contains for each custom_body its name, naif_id and radius
    :return: time_event: (list[float], size: N) Date of events in mjd format
             event_type: (list[string], size: N) Type of events
             sun_ephem_sat: (array, size: M) Ephem of the Sun from the satellite in a SkyCoord format (for EPS)
             angle_condition_res: (list[int], size: M) list of visibility condition with the Sun (for EPS)
             new_time_res: (Astropy time, size: M) Vector of time
    """
    global_event_time = []
    global_event_type = []
    print(f"\n############\tBODIES INTERVISIBILITIES START\t############")
    
    sun_ephem_sat_res = []
    angle_condition_res = []
    new_time_res = []
    
    for target in targets:
        target = name(target, custom_bodies_list)
        target_event_type = []
        target_event_time = []
        for hiding_body in hiding_bodies:

            hiding_body = name(hiding_body, custom_bodies_list)

            if target.lower() != hiding_body.lower():
                angle_condition, sat_target_ephem = IV_process(target, hiding_body, center, time_vector, sat_trajectory, custom_bodies_list)
                couple_event_time, couple_event_type = event_check(angle_condition, time_vector)
                if (
                        len(sat_trajectory) != 0
                        and minimum_status_duration != event_time_accuracy
                        and len(couple_event_type) != 0
                ):

                    sat_coord, new_time = check_event_precision(
                        couple_event_time, sat_trajectory, minimum_status_duration, event_time_accuracy
                    )
                    time_vec = np.zeros((len(new_time),2))
                    for i in range(len(new_time)):
                        time_vec[i, 0] = int(new_time[i])
                        time_vec[i, 1] = (new_time[i] - int(new_time[i])) * 86400
                    new_time = Time(new_time, format='mjd')
                    angle_condition, sat_target_ephem = IV_process(target, hiding_body, center, new_time, sat_coord, custom_bodies_list)

                    couple_event_time, couple_event_type = event_check(angle_condition, new_time)

                """"""
                target_event_type.extend(couple_event_type)
                target_event_time.extend(couple_event_time)
                """"""

        target_event_time, target_event_type = event_sort(target_event_time, target_event_type)
        target_event_time, target_event_type = event_junction(target_event_time, target_event_type)
        target_event_type = event_creation(target_event_type, types="body", target=target)

        global_event_time.extend(target_event_time)
        global_event_type.extend(target_event_type)
        print(f"Target : {target}:  Checked")
        
        if target == 'sun': 
            sun_ephem_sat = SkyCoord(CartesianRepresentation(x=sat_target_ephem[:,0], y=sat_target_ephem[:,1], z=sat_target_ephem[:,2], unit='km'))
            angle_condition_res = angle_condition
            new_time_res = new_time

    return global_event_time, global_event_type, sun_ephem_sat, angle_condition_res, new_time_res


def ground_stations_intervisibility(
        GS_number,
        GS_input,
        time_vector,
        sat_trajectory,
        hiding_bodies,
        center,
        minimum_status_duration,
        event_time_accuracy,
        custom_bodies_list,
        disable
    ):
    """
    This functions executes the entire Intervisibility process for ground stations as target
    :param GS_number: (list) Index of the selected ground stations for the computation
    :param GS_input: (dict[list]) Dict of lists containing parameters of ground stations (Name, latitude, longitude, height, minimal elevation angle, index)
    :param time_vector: (Astropy time, size: N) Vector of time
    :param sat_trajectory: (array, size: N*5) Data trajectory of the satellite,
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :param hiding_bodies: (list) Considered hiding bodies
    :param center: (string) Name of the center of the reference frame of the satellite
    :param minimum_status_duration: (float) Minimum time duration for the start and end of an event
    :param event_time_accuracy: (float) Time accuracy for detection of an event
    :param custom_bodies_list: (list[[str,int,float]], size: N*3 with N number of custom_bodies implemented) contains for each custom_body its name, naif_id and radius
    :param disable: (Boolean) Disable flag for tqdm progress bars. Default is False, use --disable to set to True
    :return: list_event_time: (list[float]) Time of events detected
             list_event_type: (list[string]) Type of events detected
    """
    print(f"\n############\tGROUND STATIONS INTERVISIBILITIES START\t############")
    
    GS_list = []
    # GS_number = info_input['ground_stations']
    for i in GS_number:
        GS_list.append(GS_input[i])

    # to remove later -> we want to know when the GS is hidden not when the earth is hidden
    # unprecise in some cases
    earth_visibility_condition = earth_occultation_status(hiding_bodies, center, time_vector, sat_trajectory, custom_bodies_list)
    
    earth_visibility_condition = np.expand_dims(earth_visibility_condition, axis=1)
    list_event_type = []
    list_event_time = []

    for actual_GS in GS_list:
        progress_bar = tqdm(total=800 + len(sat_trajectory), colour='green', leave=False, bar_format='{desc} {percentage:.0f}%|{bar}|', disable=disable)
        
        progress_bar.set_description(f"Ground Station : {actual_GS[0]}") if not progress_bar.disable else None
        
        elevation_limit = actual_GS[4]
        GS_index = actual_GS[5]
    
        # conversion of the time of the computation into SPICE time
        time_array = spice.str2et(time_vector.isot)

        earth_GS_vector = vector_GS_Earth(actual_GS, time_array)
        progress_bar.update(100) if not progress_bar.disable else None
        
        # create unit vector representing the direction of GS relative to the Earth center
        # this unit vector also represents the zenith of the GS
        unit_earth_GS_vector = np.zeros((len(sat_trajectory), 3))
        for i in range(len(earth_GS_vector)):
            unit_earth_GS_vector[i] = earth_GS_vector[i] / norm(earth_GS_vector[i, 0:3])
        progress_bar.update(100) if not progress_bar.disable else None
        
        # convert names into naif ID
        naif_id_center = spice.bods2c(str(center))

        # compute ephemerides of considered bodies
        ephem_center = spice.spkezr(str(naif_id_center), time_array, 'J2000', 'NONE', '0')
        ephem_earth = spice.spkezr('399', time_array, 'J2000', 'NONE', '0')
        progress_bar.update(100) if not progress_bar.disable else None
        
        # compute position of the GS relative to the satellite
        GS_sat_vector = np.zeros((len(sat_trajectory), 3))
        for i in range(len(earth_GS_vector)):
            GS_sat_vector[i] = - earth_GS_vector[i] - ephem_earth[0][:][i][0:3] + ephem_center[0][:][i][0:3] + sat_trajectory[i, 2:5]
            progress_bar.update(1) if not progress_bar.disable else None
        
        gamma_angle = angle_sat_zenithGS(unit_earth_GS_vector, GS_sat_vector)
        progress_bar.update(100) if not progress_bar.disable else None
        
        # True == Visibility between sat and GS == inside the sky visibility cone
        # False == no Visibility
        elevation_limit_rad = elevation_limit * np.pi /180
        visibility_cone_condition = gamma_angle < (np.pi / 2) - elevation_limit_rad
        progress_bar.update(100) if not progress_bar.disable else None
        
        # compute the occultation of the Earth by the hiding body
        visibility_cone_condition = np.expand_dims(visibility_cone_condition, axis=1)
        conditions_list = np.append(earth_visibility_condition, visibility_cone_condition, axis=1)

        GS_intervisibility_condition = np.ones((len(earth_visibility_condition)))
        
        for i in range(len(conditions_list)):
            if 0 in conditions_list[i]:
                GS_intervisibility_condition[i] = 0
        progress_bar.update(100) if not progress_bar.disable else None
        
        event_time, event_type = event_check(GS_intervisibility_condition, time_vector)
        progress_bar.update(100) if not progress_bar.disable else None
        
        # remake the calcul with a new interpolation focus around the event_times
        if (
                len(sat_trajectory) != 0
                and minimum_status_duration != event_time_accuracy
                and len(event_type) != 0
        ):
            
            new_sat_coord, new_time = check_event_precision(
                event_time, sat_trajectory, minimum_status_duration, event_time_accuracy
                
            )
            progress_bar.update(100) if not progress_bar.disable else None
            progress_bar.close()
            progress_bar = tqdm(len(earth_GS_vector), colour='green', leave = False, bar_format='{desc} {percentage:.0f}%|{bar}|', disable=disable)
            progress_bar.set_description(f"Ground Station : {actual_GS[0]}") if not progress_bar.disable else None
            
            time_vec = np.zeros((len(new_time),2))
            for i in range(len(new_time)):
                time_vec[i, 0] = int(new_time[i])
                time_vec[i, 1] = (new_time[i] - int(new_time[i])) * 86400
            new_time = Time(new_time, format='mjd')   
            
            earth_visibility_condition_2  = earth_occultation_status(hiding_bodies, center, new_time, new_sat_coord, custom_bodies_list) 
            earth_visibility_condition_2 = np.expand_dims(earth_visibility_condition_2, axis=1)
            
            new_time_array = spice.str2et(new_time.isot)
            earth_GS_vector = vector_GS_Earth(actual_GS, new_time_array)
            
            # compute ephemerides of considered bodies
            ephem_center = spice.spkezr(str(naif_id_center), new_time_array, 'J2000', 'NONE', '0')
            ephem_earth = spice.spkezr('399', new_time_array, 'J2000', 'NONE', '0')
            
            # compute position of the GS relative to the satellite
            GS_sat_vector = np.zeros((len(new_sat_coord), 3))
            for i in range(len(earth_GS_vector)):
                GS_sat_vector[i] =  - earth_GS_vector[i] - ephem_earth[0][:][i][0:3] + ephem_center[0][:][i][0:3] + new_sat_coord[i, 2:5]
                progress_bar.update(1) if not progress_bar.disable else None
            
            unit_earth_GS_vector = np.zeros((len(new_sat_coord), 3))
            for i in range(len(earth_GS_vector)):
                unit_earth_GS_vector[i] = earth_GS_vector[i] / norm(earth_GS_vector[i, 0:3]) 
            
            gamma_angle = angle_sat_zenithGS(unit_earth_GS_vector, GS_sat_vector)
            
            # True == Visibility between sat and GS == inside the sky visibility cone
            # False == no Visibility
            elevation_limit_rad = elevation_limit * np.pi /180
            visibility_cone_condition = gamma_angle < (np.pi / 2) - elevation_limit_rad

            # compute the occultation of the Earth by the hiding body
            visibility_cone_condition = np.expand_dims(visibility_cone_condition, axis=1)
            conditions_list = np.append(earth_visibility_condition_2, visibility_cone_condition, axis=1)
            
            GS_intervisibility_condition = np.ones((len(earth_visibility_condition)))

            for i in range(len(conditions_list)):
                if 0 in conditions_list[i]:
                    GS_intervisibility_condition[i] = 0

            event_time, event_type = event_check(GS_intervisibility_condition, new_time)
        com_event_type = event_creation(event_type, types='GS', index=str(GS_index))    
           
        list_event_time.extend(event_time)
        list_event_type.extend(com_event_type)
        progress_bar.close()
        print(f"Ground Station : {actual_GS[0]}:  Checked")
        
        
    progress_bar = tqdm(1, colour='green', leave = False, disable=disable) #need this or display bug
    
    return list_event_time, list_event_type


def stars_intervisibility(
        star_catalog,
        time_vector,
        sat_trajectory,
        hiding_bodies,
        center,
        minimum_status_duration,
        event_time_accuracy,
        custom_bodies_list
        ):
    """
    This functions executes the entire Intervisibility process for stars as target
    :param star_catalog: (string) Path and name of the star catalog used for the computation
    :param time_vector: (Astropy time, size: N) Vector of time
    :param sat_trajectory: (array, size: N*5) Data trajectory of the satellite,
                    [N, 0:1] time in mjd format, [N, 2:5] Position of the sat in cartesian frame
    :param hiding_bodies: (list) Considered hiding bodies
    :param center: (string) Name of the center of the reference frame of the satellite
    :param minimum_status_duration: (float) Minimum time duration for the start and end of an event
    :param event_time_accuracy: (float) Time accuracy for detection of an event
    :param custom_bodies_list: (list[[str,int,float]], size: N*3 with N number of custom_bodies implemented) contains for each custom_body its name, naif_id and radius
    :return: global_event_time: (list[float]) Time of events detected
             global_event_type: (list[float]) Type of events detected
    """
    print(f"\n############\tSTARS INTERVISIBILITIES START\t############")

    star_catalog_components = read_cic(star_catalog)

    global_event_time = []
    global_event_type = []

    for star in star_catalog_components:
        star_event_type = []
        star_event_time = []

        star_index = star[0]
        ra = star[1]
        dec = star[2]

        # print(f"Checking star {int(star_index)}")
        star_vector_direction = star_ICRS_direction(ra, dec)

        for hiding_body in hiding_bodies:
            beta_angle, sat_hiding_body_ephem = hiding_body_angle(hiding_body, center, time_vector, sat_trajectory, custom_bodies_list)

            gamma_angle = separation_angle(star_vector_direction, sat_hiding_body_ephem)

            intervisibility_criterion = gamma_angle > beta_angle
            intervisibility_criterion = [int(i) for i in intervisibility_criterion]

            couple_event_time, couple_event_type = event_check(intervisibility_criterion, time_vector)

            if (
                    len(sat_trajectory) != 0
                    and minimum_status_duration != event_time_accuracy
                    and len(couple_event_type) != 0
            ):
                new_sat_coord, new_time = check_event_precision(
                    couple_event_time, sat_trajectory, minimum_status_duration, event_time_accuracy
                )
                new_time = Time(new_time, format='mjd')
                beta_angle, sat_hiding_body_ephem = hiding_body_angle(hiding_body, center, new_time, new_sat_coord, custom_bodies_list)

                gamma_angle = separation_angle(star_vector_direction, sat_hiding_body_ephem)

                intervisibility_criterion = gamma_angle > beta_angle
                intervisibility_criterion = [int(i) for i in intervisibility_criterion]

                couple_event_time, couple_event_type = event_check(intervisibility_criterion, new_time)

            star_event_type.extend(couple_event_type)
            star_event_time.extend(couple_event_time)


        star_event_time, star_event_type = event_sort(star_event_time, star_event_type)
        star_event_time, star_event_type = event_junction(star_event_time, star_event_type)
        star_event_type = event_creation(star_event_type, "star", index=str(int(star_index)))

        global_event_time.extend(star_event_time)
        global_event_type.extend(star_event_type)

        print(f"Star index : {int(star[0])}:  Checked")

    return global_event_time, global_event_type
